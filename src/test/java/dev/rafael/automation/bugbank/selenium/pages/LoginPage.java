package dev.rafael.automation.bugbank.selenium.pages;

import org.openqa.selenium.By;

public class LoginPage extends BasePage {
	// Locators
	private By email = By.xpath("/html/body/div/div/div[2]/div/div[1]/form/div[1]/input");
	private By password = By.xpath("/html/body/div/div/div[2]/div/div[1]/form/div[2]/div/input");
	private By btnAcess = By.cssSelector("button[class='style__ContainerButton-sc-1wsixal-0 otUnI button__child']");
	
	/**
	 * Method to do account log in
	 * 
	 * @author Rafael P. Fiorin
	 * @param mail - e-mail registered
	 * @param pass - password registered
	 * @return void
	 */
	public void logIn(String mail, String pass) {		
		super.type(mail, email);
		super.type(pass, password);
		
		super.click(btnAcess);
	}
}
