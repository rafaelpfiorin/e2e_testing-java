package dev.rafael.automation.bugbank.selenium.pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
	private final static WebDriver driver = new ChromeDriver();
	private final static WebDriverWait awaitWebdriver = new WebDriverWait(driver, Duration.ofSeconds(5));

	/**
	 * Static methods to resize browser in the initialization and do wait
	 */
	static {
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
	}
	public WebDriverWait waitTo() {
		return awaitWebdriver;
	}

	/**
	 * Methods to hookers execution
	 */
	public void visit(String url) {
		driver.get(url);
	}
	public void quitWebDriver() {
		driver.quit();
	}

	/**
	 * Methods to interact with page elements
	 */
	public void type(String input, By locator) {
		driver.findElement(locator).sendKeys(input);
	}
	public void click(By locator) {
		driver.findElement(locator).click();
	}

	/**
	 * Method to return some page element
	 */
	public WebElement element(By locator) {
		return driver.findElement(locator);
	}
}