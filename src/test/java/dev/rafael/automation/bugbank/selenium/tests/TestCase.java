package dev.rafael.automation.bugbank.selenium.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dev.rafael.automation.bugbank.selenium.pages.LoginPage;
import dev.rafael.automation.bugbank.selenium.pages.RegisterPage;
import dev.rafael.automation.bugbank.selenium.pages.TransferPage;
import io.github.cdimascio.dotenv.Dotenv;

class TestCase {
	// Invokes the *.env file for account passwords
	Dotenv dotenv = Dotenv.load();
	// Class instance attributes
	private RegisterPage registerPage;
	private LoginPage loginPage;
	private TransferPage transferPage;
	// Constant attributes
	private final String HOME_URL = "https://bugbank.netlify.app/";
	private final Double initalBalanceOfFirstAccount = 1000.00;

	@BeforeEach
	void setUp() {
		// Web driver instantiation and home page visit
		registerPage = new RegisterPage();
		registerPage.visit(this.HOME_URL);
	}

	@Test
	public void accountCreationAndTransfer() throws InterruptedException {
		// Loads the accounts passwords from *.env file
		String senha1 = dotenv.get("SENHA_1");
		String senha2 = dotenv.get("SENHA_2");

		// Executes the first account registration
		registerPage.registerAccount("rpfiorin@outlook.com", "Rafael", senha1, senha1);

		// Storages the number account details to receive the transfer
		String accountDestination = registerPage.getAccount();
		String accountDestinationDigit = registerPage.getAccountDigit();

		// Cleans the filled fields
		registerPage.cleanRegisterFields();

		// Executes the second account registration
		registerPage.registerAccount("rpfiorin28@gmail.com", "Fiorin", senha2, senha2);
		registerPage.closeToLogin();

		// Does the login with the second account registered
		loginPage = new LoginPage();
		loginPage.logIn("rpfiorin28@gmail.com", senha2);

		// Converts the value to be transfered
		String accountValueTransfer = "100.55";
		Double accountValueTransfered = Double.parseDouble(accountValueTransfer);

		// Gets the initial balance of second account
		transferPage = new TransferPage();
		Double initialBalanceSecondAccount = transferPage.accountBalance();

		// Does the transfer operation to first account registered
		transferPage.transfer(accountDestination, accountDestinationDigit, accountValueTransfer,
				"Transferência a Rafael");

		// Does the assertion of remaining balance
		assertEquals((initialBalanceSecondAccount - accountValueTransfered), transferPage.accountBalance(),
				"Verificando saldo restante");

		// Exits from second account
		transferPage.logOut();

		// Does log in with the first account registered
		loginPage.logIn("rpfiorin@outlook.com", senha1);

		// Does the assertion of total balance from this account
		Double lastBalanceSecondAccount = transferPage.accountBalance();
		assertEquals((lastBalanceSecondAccount), initalBalanceOfFirstAccount + accountValueTransfered, 
				"Verificando saldo com transferência recebida");
	}

	@AfterEach
	void tearDown() throws Exception {
		// Ends driver instantiation
		registerPage.quitWebDriver();
	}
}
