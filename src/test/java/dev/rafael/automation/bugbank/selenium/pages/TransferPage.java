package dev.rafael.automation.bugbank.selenium.pages;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class TransferPage extends BasePage {
	// Locators
	private By balance = By.id("textBalance");
	private By btnGoToTransfer = By.id("btn-TRANSFERÊNCIA");
	private By btnGoBackTransfer = By.id("btnBack");
	private By btnExit = By.id("btnExit");
	private By btnFinishTransfer = By.id("btnCloseModal");
	private By btnTransfer = By.xpath("/html/body/div/div/div[3]/form/button");
	private By destinationAccount = By.xpath("/html/body/div/div/div[3]/form/div[1]/div[1]/input");
	private By destinationAccountDigit = By.name("digit");
	private By transferValue = By.name("transferValue");
	private By transferDescription = By.name("description");
	private By transferMessage = By.id("modalText");

	/**
	 * Method to do get account balance
	 * 
	 * @author Rafael P. Fiorin
	 * @param no params
	 * @return balance formatted
	 */
	public Double accountBalance() {
		String actualBalance = super.element(balance).getText();
		String newBalance = actualBalance.replaceAll("[^0-9]", "");

		StringBuilder builder = new StringBuilder(newBalance);
		builder.insert(newBalance.length() - 2, ".");

		String finalBalance = builder.toString();
		return Double.parseDouble(finalBalance);
	}

	/**
	 * Method to do transfer operation
	 * 
	 * @author Rafael P. Fiorin
	 * @param destination      - number of account destination
	 * @param destinationDigit - digit of account destination
	 * @param value            - transfer value
	 * @param description      - some text
	 * @return void
	 */
	public void transfer(String destination, String destinationDigit, String value, String description) {
		super.click(btnGoToTransfer);
		
		super.type(destination, destinationAccount);
		super.type(destinationDigit, destinationAccountDigit);
		super.type(value, transferValue);
		super.type(description, transferDescription);
		super.click(btnTransfer);

		super.waitTo().until(ExpectedConditions.elementToBeClickable(btnFinishTransfer));
		assertEquals("Transferencia realizada com sucesso", super.element(transferMessage).getText(), "Verificando a mensagem");

		super.click(btnFinishTransfer);
		super.click(btnGoBackTransfer);
	}

	/**
	 * Method to do account log out
	 */
	public void logOut() {
		super.click(btnExit);
	}
}
