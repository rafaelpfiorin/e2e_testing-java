package dev.rafael.automation.bugbank.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class RegisterPage extends BasePage {
	// Locators
	private By previousPage = By.id("btnBackButton");
	private By goToRegistration = By.cssSelector("button[class='style__ContainerButton-sc-1wsixal-0 ihdmxA button__child']");
	private By email = By.xpath("/html/body/div/div/div[2]/div/div[2]/form/div[2]/input");
	private By name = By.name("name");
	private By pass = By.xpath("/html/body/div/div/div[2]/div/div[2]/form/div[4]/div/input");
	private By confirmationPass = By.xpath("/html/body/div/div/div[2]/div/div[2]/form/div[5]/div/input");
	private By toggle = By.id("toggleAddBalance");
	private By registration = By.cssSelector("button[class='style__ContainerButton-sc-1wsixal-0 CMabB button__child']");
	private By numberAccount = By.id("modalText");
	private By closeMessage = By.xpath("/html/body/div/div/div[3]/div/div[1]/a");

	/**
	 * Method to do an account register
	 * 
	 * @author Rafael P. Fiorin
	 * @param emailAdress  - E-mail to login
	 * @param personName   - Name of person
	 * @param password     - Password chosen
	 * @param passwordConf - Password confirmation
	 * @return void
	 */
	public void registerAccount(String emailAdress, String personName, String password, String passwordConf) {
		super.click(goToRegistration);

		super.type(emailAdress, email);
		super.type(personName, name);
		super.type(password, pass);
		super.type(passwordConf, confirmationPass);

		try {
			Thread.sleep(500);
			super.click(toggle);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		super.click(registration);
	}

	/**
	 * Methods to get the account details
	 * 
	 * @author Rafael P. Fiorin
	 * @param no params
	 * @return void
	 */
	public String getAccount() {
		super.waitTo().until(ExpectedConditions.visibilityOfElementLocated(numberAccount));
		String textAccount = super.element(numberAccount).getText();
		String account = textAccount.replaceAll("[^0-9]", "");

		return account.substring(0, account.length() - 1);
	}
	public String getAccountDigit() {
		String textAccount = super.element(numberAccount).getText();
		String account = textAccount.replaceAll("[^0-9]", "");
		String accountDigit = account.substring(account.length() - 1);

		closeToLogin();
		return accountDigit;
	}

	/**
	 * Method to close register confirmation message
	 * 
	 * @author Rafael P. Fiorin
	 * @param no params
	 * @return void
	 */
	public void closeToLogin() {
		super.waitTo().until(ExpectedConditions.elementToBeClickable(closeMessage));
		
		super.click(closeMessage);
	}

	/**
	 * Method to clean the register filled fields
	 * 
	 * @author Rafael P. Fiorin
	 * @param no params
	 * @return void
	 */
	public void cleanRegisterFields() {
		super.click(goToRegistration);

		super.element(email).clear();
		super.element(name).clear();
		super.element(pass).clear();
		super.element(confirmationPass).clear();

		try {
			Thread.sleep(500);
			super.click(toggle);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		super.click(previousPage);
	}
}
