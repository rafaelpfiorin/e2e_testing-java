# Automação de testes front-end da aplicação 'BugBank'

## Como executar
1. Clone este projeto com opção HTTPS ou SSH.
2. A automação foi desenvolvida para Chromium browser. Logo, certifique-se de ter o arquivo chromedriver presente em diretório path (como pasta Windows) na versão compatível com seu navegador.
3. Após clonado, abra o arquivo .env presente na raíz e escolha as senhas para cadastro das contas no site (se desejado personalizar) e salve-o.
4. Para execução direta e conferência do report gerado, certifique-se de ter o maven presente em seu SO.
5. Com maven instalado/configurado, acesse a pasta do projeto pelo terminal e execute o comando: 
   mvn clean test site
	
6. Os testes serão executados automaticamente e o report estará presente em target\site\index.html, no link 'Project Reports'. O projeto foi feito conforme enunciado do desafio, então, o report 
indicará o resultado de um caso de teste.

OBS: O 'BugBank' pode sofrer de instabilidades inesperadas, caso ocorra alguma quebra na execução, repita-a. 
Aprecie!